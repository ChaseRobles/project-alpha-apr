from django.urls import path
from .views import project_list, details, create_project

urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
