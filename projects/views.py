from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def project_list(request):
    lists = Project.objects.filter(owner=request.user.id)
    context = {
        "lists": lists,
    }
    return render(request, "projects/project_list.html", context)


def details(request, id):
    items = Project.objects.all()
    lists = get_object_or_404(Project, id=id)
    context = {"items": items, "lists": lists}
    return render(request, "projects/details.html", context)


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():

            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)


# Create your views here.
